import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { sideMenu } from '../../config';
import { SideMenuWithChild } from '../../interfaces';
import { convertToSentenceText } from '../../helpers';
import { useAuthSelector } from '../../custom-hook';
import { BaseCard } from '../../cards';
import MenuItem from '../menu-item';
import MenuItemCollapsable from '../menu-item-collapsable';
import avatar from '../../assets/img/default-user.svg';
import './SideBar.scss';

function SideBar(props: RouteComponentProps) {
  const { resAuth } = useAuthSelector();

  if (!resAuth) return null;

  const pathname: string = props.location.pathname;
  let find = sideMenu.findIndex((item: SideMenuWithChild) => pathname.toLowerCase() === item.path.toLowerCase());
  if (find === -1) find = sideMenu.findIndex((item: SideMenuWithChild) => pathname.toLowerCase().includes(item.path.toLowerCase()));

  return (
    <BaseCard cardColClassName="sticky-side-bar w-100 p-0" cardClassName="br-10" colSizes={{ lg: 12, md: 12, sm: 12, xs: 12 }}>
      <Row className="sidebar-head ml-0 mr-0 mb-4 pb-3 pt-3">
        <Col lg={12} md={12} sm={12} xs={12}>
          <div className="content-center">
            <img src={avatar} alt="avatar" className="w-50" />
          </div>
          <div className="text-center pt-2 font-size-16 company-color-yellow">
            <b>
              {convertToSentenceText(resAuth.name)}
            </b>
          </div>
        </Col>
      </Row>
      <Row className="m-0 min-h-20vh">
        <Col lg={12} md={12} sm={12} xs={12} className="p-0">
          <div className="sidebar-tab-wrapper">
            {
              sideMenu.map((obj: SideMenuWithChild, index: number) => {
                // will use to check if user has role to access the menu
                // const isMenuDisplayed = res.auth.roles.find((item: string) => obj.accessRole === item);
                // if (!isMenuDisplayed) return null;

                const isActive = index === find;

                if (obj.children) {
                  return (
                    <MenuItemCollapsable key={index} obj={obj} pathname={pathname} />
                  );
                }

                return (
                  <MenuItem isActive={isActive} obj={obj} key={index} />
                );
              })
            }
          </div>
        </Col>
      </Row>
    </BaseCard>
  );
}

export default withRouter(SideBar);
