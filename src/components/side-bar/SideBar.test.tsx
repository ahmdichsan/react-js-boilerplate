import React from 'react';
import { Provider } from 'react-redux';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../constants';
import Sidebar from '.';

const mockStore = configureMockStore();
const store = mockStore({ auth: initialState });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <Sidebar />
      </BrowserRouter>
    </Provider>,
    div,
  );
});
