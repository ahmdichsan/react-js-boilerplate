import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Navbar, NavbarToggler, Collapse,
  Nav, NavItem, NavLink, Button,
} from 'reactstrap';
import { userLogout } from '../../redux-global/Global.actions';
import ecomindoLogoFullBlue from '../../assets/icon/ecomindo-logo-full-blue.svg';
import './TopBar.scss';

export default function TopBar() {
  const dispatch = useDispatch();

  const [isOpen, setIsOpen] = useState(false);

  const toggleNav = () => setIsOpen(!isOpen);

  const logout = () => dispatch(userLogout());

  return (
    <Navbar expand="md" className="navbar-wrapper">
      <img src={ecomindoLogoFullBlue} alt="ecom" className="w-150px" />
      <NavbarToggler
        onClick={toggleNav}
        data-testid="navbar-toggler"
        className="navbar navbar-light outline-none"
      />
      <Collapse isOpen={isOpen} navbar data-testid="navbar-collapse">
        <Nav className="ml-auto" navbar>
          <NavItem className="ml-auto navbar-navitem">
            <NavLink href="#" className="pr-0">
              <Button className="button-logout" type="button" onClick={logout}>
                Sign Out
              </Button>
            </NavLink>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
}
