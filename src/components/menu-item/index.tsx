import React from 'react';
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { MenuItemProps } from '../../interfaces';

function MenuItem(props: MenuItemProps) {
  const { obj, isActive } = props;

  const changeTab = () => props.history.push(obj.path);

  return (
    <Row className="p-0 m-0 w-100" onClick={changeTab} data-testid="menu-item" id="menu-item">
      <Col
        md={12} lg={12} sm={12} xs={12}
        className={`h-40px p-0 ${isActive ? 'sidebar-tab-label-selected' : ''}`}
      >
        <Row className="w-100 m-0 p-0">
          <Col md={1} lg={1} sm={1} xs={1} className="p-0 let-content">
            <div className={isActive ? "active-sign" : 'inactive-sign'} />
          </Col>
          <Col md={11} lg={11} sm={11} xs={11} className="font-size-13 left-content">{obj.menu}</Col>
        </Row>
      </Col>
    </Row>
  );
}

export default withRouter(MenuItem);
