import React from 'react';
import { Provider } from 'react-redux';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../constants';
import { SideMenu } from '../../interfaces';
import MenuItem from '.';
import timelineIcon from '../../assets/icon/timeline.svg';

const mockStore = configureMockStore();
const store = mockStore({ auth: initialState });

const obj: SideMenu = {
  accessRole: 'example',
  icon: timelineIcon,
  menu: 'Add Movie',
  path: '/movies/add'
};

const Component = () => (
  <Provider store={store}>
    <BrowserRouter>
      <MenuItem isActive={true} obj={obj} />
    </BrowserRouter>
  </Provider>
);

describe('<MenuItem />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Component />, div);
  });
});
