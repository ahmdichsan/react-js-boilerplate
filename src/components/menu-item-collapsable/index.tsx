import React, { Fragment } from 'react';
import { Row, Col, UncontrolledCollapse } from 'reactstrap';
import { MenuItemCollapsableProps, SideMenu } from '../../interfaces';
import MenuItem from '../menu-item';

export default function MenuItemCollapsable(props: MenuItemCollapsableProps) {
  const { obj, pathname } = props;
  if (!obj.children) return null;

  const findChild = obj.children.findIndex((item: SideMenu) => pathname.toLowerCase().includes(item.path.toLowerCase()));

  return (
    <Fragment>
      <Row id={obj.menu} className="m-0 w-100">
        <Col md={12} lg={12} sm={12} xs={12} className="h-40px pr-0 pl-0">
          <Row className="m-0 w-100">
            <Col md={2} lg={2} sm={2} xs={2} className="p-0">
              <img src={obj.icon} className="w-25px" alt="icon" />
            </Col>
            <Col md={8} lg={8} sm={8} xs={8} className="left-content font-size-12">
              {obj.menu}
            </Col>
            <Col md={1} lg={1} sm={1} xs={1} className="right-content pr-0">
              <i className="fa fa-caret-down p-0" />
            </Col>
          </Row>
        </Col>
      </Row>

      <UncontrolledCollapse toggler={`#${obj.menu}`} defaultOpen={findChild > -1} className="w-100">
        {
          obj.children.map((objChild: SideMenu, indexChild: number) => {
            // will use to check if user has role to access the menu
            // const isMenuChildDisplayed = resAuth.roles.find((item) => objChild.accessRole === item);
            // if (!isMenuChildDisplayed) return null;

            const isActive = findChild === indexChild;

            return (
              <MenuItem isActive={isActive} obj={objChild} key={indexChild} />
            );
          })
        }
      </UncontrolledCollapse>
    </Fragment>
  );
}
