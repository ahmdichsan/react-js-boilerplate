import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import { SideMenuWithChild } from '../../interfaces';
import MenuItemCollapsable from '.';
import timelineIcon from '../../assets/icon/timeline.svg';

const obj: SideMenuWithChild = {
  accessRole: 'example',
  icon: timelineIcon,
  menu: 'Add',
  path: '/',
  children: [
    {
      accessRole: 'example',
      icon: timelineIcon,
      menu: 'Add Movie',
      path: '/movies/add',
    }
  ]
};

const Component = () => (
  <BrowserRouter>
    <MenuItemCollapsable obj={obj} pathname="/movies/add" />
  </BrowserRouter>
);

describe('<MenuItemCollapsable />', () => {
  it('renders without crashing', () => {
    render(<Component />);
  });
});
