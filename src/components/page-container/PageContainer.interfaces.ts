export declare interface PageContainerProps {
  children: React.ReactNode;
}