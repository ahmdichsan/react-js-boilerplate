//  Region Import External Lib (e.g React, Reactstrap, etc)
import React from 'react';

//  Region Import Constants


//  Region Import Interfaces
import { PageContainerProps } from './PageContainer.interfaces';

//  Region Import Redux Action Type and Redux Action


//  Region Import Helper Function


//  Region Import Custom Hook


//  Region Import Components/Cards


//  Region Import Assets


//  Region Import Style
import './PageContainer.scss';

export default function PageContainer(props: PageContainerProps) {
  //  useDispatch declaration

  //  useState declaration

  //  props object destruction

  //  useCustomHook

  //  onHandleFunction declaration

  //  useEffect declaration

  //  return element
  return (
    <div className="pt-4 w-100 pr-3">
      {props.children}
    </div>
  );
}
