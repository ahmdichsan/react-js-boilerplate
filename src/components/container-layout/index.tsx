import React, { useEffect, Suspense } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Row, Col } from 'reactstrap';

import { routerContainer } from '../../config';

import { HistoryProps } from '../../interfaces';

import { HIDE_SIDEBAR, SHOW_SIDEBAR, updateNetwork } from '../../redux-global/Global.actions';
import { syncToDoFetch } from '../../views/to-do/ToDo.actions';

import { useAuthSelector, useSidebarVisibilitySelector, useNetwork, usePrevious } from '../../custom-hook';

import { SimpleLoading, Footer, SideBar } from '..';
import TopBar from '../top-bar';

function ContainerLayout(props: HistoryProps) {
  const dispatch = useDispatch();
  
  const { resAuth } = useAuthSelector();
  const { actionSidebar } = useSidebarVisibilitySelector();

  const { isOnline } = useNetwork();
  const previousOnlineStatus = usePrevious(isOnline);

  useEffect(() => {
    if (previousOnlineStatus !== isOnline) {
      dispatch(updateNetwork(isOnline));
    }

    // call sync
    if (!previousOnlineStatus && isOnline) {
      dispatch(syncToDoFetch());
    }

  }, [dispatch, isOnline, previousOnlineStatus]);

  useEffect(() => {
    if (!resAuth) {
      props.history.push('/login');
    }
  }, [resAuth, props.history]);

  let sidebarClassName = 'pt-4 w-18-percent';
  let bodyClassName = 'p-0';

  switch (actionSidebar) {
    case HIDE_SIDEBAR:
      sidebarClassName = 'display-none';
      bodyClassName += ' w-100';
      break;
    case SHOW_SIDEBAR:
      bodyClassName += ' w-82-percent';
      break;

    default:
      break;
  }

  return (
    <div className="app min-h-100vh">
      <TopBar />
      <Row className="min-h-65vh m-0 d-flex">
        <Col md="auto" className={sidebarClassName}>
          <SideBar />
        </Col>
        <Col md="auto" className={bodyClassName}>
          <Suspense fallback={<SimpleLoading />}>
            <Switch>
              {routerContainer.map((route, idx) =>
                route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    component={route.component}
                  />
                ) : null,
              )}
              <Redirect from="/" to="/login" />
            </Switch>
          </Suspense>
        </Col>
      </Row>
      <Footer />
    </div>
  );
}

export default withRouter(ContainerLayout);
