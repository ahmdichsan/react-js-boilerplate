import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { shallow } from 'enzyme';
import ContainerLayout from '.';

const Component = () => (
  <BrowserRouter>
    <ContainerLayout />
  </BrowserRouter>
);

it('renders without crashing', () => {
  shallow(<Component />);
});
