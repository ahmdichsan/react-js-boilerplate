import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { companyName } from '../../constants';
import './Footer.scss';

export default function Footer() {
  return (
    <footer className="footer-wrapper">
      <Row className="footer-wrapper-row grey-bg pt-4 pb-4">
        <Col md={3} lg={3} xs={12} sm={12}>
          <ul className="list-style-type-none footer-ul">
            <li className="footer-li">
              <div className="float-left">
                <span className="display-block">
                  Gran Rubina Business Park 18th Floor,
                </span>
                <span className="display-block">
                  JL. H.R. Rasuna Said Kav C-22
                </span>
                <span className="display-block">
                  Jakarta Selatan, 12940
                </span>
              </div>
            </li>
            <li className="footer-li clear-both">
              <div className="float-left">
                <span className="display-block">
                  +6221&nbsp;2039&nbsp;2036
                </span>
              </div>
            </li>
            <li className="footer-li clear-both">
              <div className="float-left">
                <span className="display-block">
                  info@ecomindo.com
                </span>
              </div>
            </li>
          </ul>
        </Col>

        <Col md={2} lg={2} xs={12} sm={12}>
          <ul className="list-style-type-none footer-ul">
            <li className="footer-li mar-bot-10">
              <b>GET HELP</b>
            </li>
            <li className="footer-li">
              <Link to="/" className="color-white">
                Help &amp; FAQ
              </Link>
            </li>
            <li className="footer-li">
              <Link to="/" className="color-white">
                Online Support
              </Link>
            </li>
          </ul>
        </Col>
      </Row>

      <Row className="footer-wrapper-row pt-1 pb-1">
        <Col md={12} lg={12} xs={12} sm={12}>
          <div className="center-content company-color-yellow min-height-35">
            Copyrights 2020 &reg; {companyName} Allrights Reserved
          </div>
        </Col>
      </Row>
    </footer>
  );
}
