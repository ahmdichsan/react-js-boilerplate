import 'dotenv/config';

export const initialState = {
  fetch: false, // set to true/false if its related with api call
  data: null, // payload here
  res: null, // response for success action here. if not related to API cal, put value here
  err: null, // response for failed action here
  action: '',
};

export const initialAction = {
  type: 'DEFAULT',
  data: null,
};

export const companyName = 'eComindo';
