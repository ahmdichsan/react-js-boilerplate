import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { rootReducer } from './App.reducers';
import appSagas from './App.sagas';
import { configPersist } from '../config';

const finalReducers = persistReducer(configPersist, rootReducer);
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

const sagaMiddleware = createSagaMiddleware();

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middlewares = process.env.NODE_ENV === 'development' ? [sagaMiddleware, logger] : [sagaMiddleware];

const reduxStore = createStore(finalReducers, composeEnhancer(applyMiddleware(...middlewares)));

sagaMiddleware.run(appSagas);

/**
 * note: in line 38, without "any", it will compiled error with error:
 * Conversion of type 'import("redux-persist").Persistor' to type 'import("redux-persist/es/types").Persistor'
 * 
 * this is related to this PR: https://github.com/rt2zz/redux-persist/pull/1085#issue-315212260
 * 
 * if the PR has merged, the "any" type in line 38 can be omitted.
 */

const persistor: any = persistStore(reduxStore);
const store = reduxStore;

export { persistor, store };
