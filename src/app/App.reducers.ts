import { combineReducers } from 'redux';
import { configReducer, hookReducer } from '../config';

const reduxReducer: any = combineReducers(configReducer);

export type AppState = ReturnType<typeof reduxReducer>

export const rootReducer: any = (state: AppState, action: any) => {
  const hook = hookReducer(state, action);
  return reduxReducer(hook.state, hook.action);
};
