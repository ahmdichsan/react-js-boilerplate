import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './App.store';
import { routerNonContainer } from '../config';
import { SimpleLoading, ContainerLayout } from '../components';

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Suspense fallback={<SimpleLoading />}>
            <Switch>
              {routerNonContainer.map((route, idx) =>
                route.component ? (
                  <Route key={idx} path={route.path} exact={route.exact} component={route.component} />
                ) : null,
              )}
              <Route path="/" component={ContainerLayout} />
            </Switch>
          </Suspense>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}
