import { all } from 'redux-saga/effects';
import configSaga from '../config/Config.sagas';

export default function* appSagas() {
  if (configSaga) yield all(configSaga);
}
