import { useState, useEffect } from 'react';

export function useNetwork(): { isOnline: boolean } {
  const [isOnline, setNetwork] = useState(window.navigator.onLine);

  async function updateNetwork() {
    const condition = window.navigator.onLine ? 'online' : 'offline';

    if (condition === 'online') {
      /**
       * try to ping google to make sure the connection exist or not
       */
      await fetch('//google.com', { mode: 'no-cors' })
        .then(() => {
          setNetwork(true);
        })
        .catch(() => {
          setNetwork(false);
        });

      return true;
    }

    setNetwork(false);
    return true;
  }

  useEffect(() => {
    window.addEventListener("offline", updateNetwork);
    window.addEventListener("online", updateNetwork);

    return () => {
      window.removeEventListener("offline", updateNetwork);
      window.removeEventListener("online", updateNetwork);
    };
  });

  return { isOnline };
}
