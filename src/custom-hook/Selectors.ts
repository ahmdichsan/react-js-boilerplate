import { useSelector } from 'react-redux';
import { AppState } from '../app/App.reducers';
import { Auth } from '../interfaces';
import { ToDo } from '../views/to-do/interfaces';

export function useAuthSelector<Data = any>() {
  const { fetchAuth, dataAuth, resAuth, errAuth, actionAuth } = useSelector((state: AppState) => ({
    fetchAuth: state.auth ? state.auth.fetch as boolean : null,
    dataAuth: state.auth ? state.auth.data as Data : null,
    resAuth: state.auth ? state.auth.res as Auth : null,
    errAuth: state.auth ? state.auth.err : null,
    actionAuth: state.auth ? state.auth.action as string : null,
  }));

  return { fetchAuth, dataAuth, resAuth, errAuth, actionAuth };
}

export function useSidebarVisibilitySelector() {
  const { actionSidebar } = useSelector((state: AppState) => ({
    actionSidebar: state.sidebarVisibility ? state.sidebarVisibility.action : null,
  }));

  return { actionSidebar };
}

export function useToDoSelector<Data = any>() {
  const { fetchToDo, dataToDo, resToDo, errToDo, actionToDo } = useSelector((state: AppState) => ({
    fetchToDo: state.toDo ? state.toDo.fetch as boolean : null,
    dataToDo: state.toDo ? state.toDo.data as Data : null,
    resToDo: state.toDo ? state.toDo.res as ToDo[] : null,
    errToDo: state.toDo ? state.toDo.err : null,
    actionToDo: state.toDo ? state.toDo.action as string : null,
  }));

  return { fetchToDo, dataToDo, resToDo, errToDo, actionToDo };
}

export function useAddToDoSelector() {
  const { fetchAddToDo, dataAddToDo, resAddToDo, errAddToDo, actionAddToDo } = useSelector((state: AppState) => ({
    fetchAddToDo: state.addToDo ? state.addToDo.fetch as boolean : null,
    dataAddToDo: state.addToDo ? state.addToDo.data as ToDo : null,
    resAddToDo: state.addToDo ? state.addToDo.res as string : null,
    errAddToDo: state.addToDo ? state.addToDo.err : null,
    actionAddToDo: state.addToDo ? state.addToDo.action as string : null,
  }));

  return { fetchAddToDo, dataAddToDo, resAddToDo, errAddToDo, actionAddToDo };
}

export function useMarkAsDoneToDoSelector() {
  const { fetchMarkAsDoneToDo, dataMarkAsDoneToDo, resMarkAsDoneToDo, errMarkAsDoneToDo, actionMarkAsDoneToDo } = useSelector((state: AppState) => ({
    fetchMarkAsDoneToDo: state.markAsDoneToDo ? state.markAsDoneToDo.fetch as boolean : null,
    dataMarkAsDoneToDo: state.markAsDoneToDo ? state.markAsDoneToDo.data as ToDo : null,
    resMarkAsDoneToDo: state.markAsDoneToDo ? state.markAsDoneToDo.res as string : null,
    errMarkAsDoneToDo: state.markAsDoneToDo ? state.markAsDoneToDo.err : null,
    actionMarkAsDoneToDo: state.markAsDoneToDo ? state.markAsDoneToDo.action as string : null,
  }));

  return { fetchMarkAsDoneToDo, dataMarkAsDoneToDo, resMarkAsDoneToDo, errMarkAsDoneToDo, actionMarkAsDoneToDo };
}

export function useSyncToDoSelector() {
  const { fetchSyncToDo, dataSyncToDo, resSyncToDo, errSyncToDo, actionSyncToDo } = useSelector((state: AppState) => ({
    fetchSyncToDo: state.syncToDo ? state.syncToDo.fetch as boolean : null,
    dataSyncToDo: state.syncToDo ? state.syncToDo.data : null,
    resSyncToDo: state.syncToDo ? state.syncToDo.res : null,
    errSyncToDo: state.syncToDo ? state.syncToDo.err : null,
    actionSyncToDo: state.syncToDo ? state.syncToDo.action as string : null,
  }));

  return { fetchSyncToDo, dataSyncToDo, resSyncToDo, errSyncToDo, actionSyncToDo };
}

export function useNetworkSelector() {
  const { isOnline } = useSelector((state: AppState) => ({
    isOnline: state.network ? state.network.res as boolean : null,
  }));

  return { isOnline };
}
