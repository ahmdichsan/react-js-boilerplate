import { useRef, useEffect } from 'react';

export * from './Selectors';
export * from './Network';
export * from './LocalStorage';

export function usePrevious<T extends {}>(value: T): T | undefined {
  const ref = useRef<T>();

  useEffect(() => {
    ref.current = value;
  });

  return ref.current;
}
