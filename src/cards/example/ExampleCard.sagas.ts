import { call, put, takeLatest } from 'redux-saga/effects';
import { EXAMPLE_FETCH, fetchExampleFailed, fetchExampleSuccess } from './ExampleCard.actions';
import { HttpService } from '../../helpers';
import { EXAMPLEAPI } from './ExampleCard.constants';

function* workerSagaExample() {
  try {
    const response = yield call(HttpService.get, EXAMPLEAPI);

    yield put(fetchExampleSuccess(response.data));
  } catch (error) {
    console.log(error.message);
    yield put(fetchExampleFailed(error.message));
  }
}

export const watcherSagaExample = [takeLatest(EXAMPLE_FETCH, workerSagaExample)];
