import React from 'react';
import * as ReactDOM from 'react-dom';
import ExampleCard from '.';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ExampleCard />, div);
});
