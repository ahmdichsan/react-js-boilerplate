import React from 'react';
import * as ReactDOM from 'react-dom';
import Paging from './Paging';

const mockFunction = jest.fn();

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Paging
      onClickNext={mockFunction}
      onClickPage={mockFunction}
      onClickPrev={mockFunction}
      page={1}
      totalPages={1}
    />,
    div,
  );
});
