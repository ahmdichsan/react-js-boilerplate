import React from 'react';
import { PagingProps } from '../../interfaces';
import BaseCard from '../base-card/BaseCard';
import './Paging.scss';

export default function Paging(props: PagingProps) {
  return (
    <BaseCard cardClassName="paging-root" colSizes={{ md: 3, lg: 3, sm: 12, xs: 12 }}>
      <div className="pagination-container">
        {props.page > 1 && (
          <div className="inactive-page" onClick={props.onClickPrev}>&lt;</div>
        )}
        {props.page - 3 > 0 && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page - 3)}>{props.page - 3}</div>
        )}
        {props.page - 2 > 0 && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page - 2)}>{props.page - 2}</div>
        )}
        {props.page - 1 > 0 && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page - 1)}>{props.page - 1}</div>
        )}
        <div className="active-page">{props.page}</div>
        {props.page + 1 <= props.totalPages && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page + 1)}>{props.page + 1}</div>
        )}
        {props.page + 2 <= props.totalPages && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page + 2)}>{props.page + 2}</div>
        )}
        {props.page + 3 <= props.totalPages && (
          <div className="inactive-page" onClick={() => props.onClickPage(props.page + 3)}>{props.page + 3}</div>
        )}
        {props.page < props.totalPages && (
          <div className="inactive-page" onClick={props.onClickNext}>&gt;</div>
        )}
      </div>
    </BaseCard>
  );
}
