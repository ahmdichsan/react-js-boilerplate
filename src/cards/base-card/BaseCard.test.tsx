import React from 'react';
import * as ReactDOM from 'react-dom';
import BaseCard from './BaseCard';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <BaseCard>
      <div>test</div>
    </BaseCard>,
    div,
  );
});
