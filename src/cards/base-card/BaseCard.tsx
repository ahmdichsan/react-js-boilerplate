import React from 'react';
import { Card, Col } from 'reactstrap';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { BaseCardProps } from './BaseCard.interface';
import './BaseCard.scss';

function BaseCard(props: BaseCardProps) {
  const {
    cardClassName,
    id,
    colSizes,
    cardColClassName,
    name,
  } = props;

  let lg = 12;
  let md = 12;
  let sm = 12;
  let xs = 12;

  if (colSizes) {
    lg = colSizes.lg;
    md = colSizes.md;
    sm = colSizes.sm;
    xs = colSizes.xs;
  }

  const cardClassGroup = classNames(
    'card-custom',
    cardClassName,
  );

  const cardColClassGroup = classNames(
    'col-card-custom',
    cardColClassName,
  );

  const onClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    if (!props.onClick) return;

    props.onClick(event);
  };

  return (
    <Col md={md} lg={lg} sm={sm} xs={xs} className={cardColClassGroup}>
      <Card className={cardClassGroup} id={id} onClick={onClick} name={name}>
        {props.children}
      </Card>
    </Col>
  );
}

BaseCard.propTypes = {
  cardClassName: PropTypes.string,
};

BaseCard.defaultProps = {
  cardClassName: '',
};

export default BaseCard;
