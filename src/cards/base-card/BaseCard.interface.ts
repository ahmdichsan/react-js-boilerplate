import { ColSizes } from '../../interfaces';

export declare interface BaseCardProps {
  children: React.ReactNode;
  colSizes?: ColSizes;
  cardClassName?: string;
  cardColClassName?: string;
  id?: string;
  name?: string;
  onClick?(event: React.MouseEvent<HTMLElement, MouseEvent>): void;
}
