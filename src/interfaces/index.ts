import React from 'react';
import { History } from 'history';
import { RouteComponentProps } from 'react-router-dom';

export interface Auth {
  id: string;
  jwt: {
    token: string;
    expired: string;
  };
  name: string;
  userName: string;
}

export interface Profile {
  id: string;
  name: string;
  userName: string;
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string | null;
  lastUpdatedDate: string;
  activeFlag: boolean;
}

export interface State<Data = any, Res = any> {
  fetch: boolean;
  data: Data;
  res: Res;
  err: any;
  action: string;
}

export interface Action<Data = any> {
  type: string;
  data: Data;
}

export interface HistoryProps {
  history: History;
}

export interface AuthSelector extends Omit<State, 'res'> {
  res: Auth;
}

export interface ColSizes {
  md: number;
  lg: number;
  sm: number;
  xs: number;
}

export interface SideMenu {
  menu: string;
  path: string;
  icon: any;
  accessRole: string;
}

export interface SideMenuWithChild extends SideMenu {
  children?: SideMenu[];
}

export interface MenuItemProps extends HistoryProps, RouteComponentProps {
  isActive: boolean;
  obj: SideMenu;
}

export interface MenuItemCollapsableProps {
  pathname: string;
  obj: SideMenuWithChild;
}

export interface PagingProps {
  page: number;
  totalPages: number;
  onClickPage: (page: number) => void;
  onClickNext: () => void;
  onClickPrev: () => void;
}

export declare interface DefaultRouter {
  path: string;
  name: string;
  component?: React.LazyExoticComponent<any>;
  exact?: boolean;
}
