export const USER_LOG_OUT = 'USER_LOG_OUT';

export const userLogout = () => ({ type: USER_LOG_OUT });
