export const UPDATE_NETWORK = 'UPDATE_NETWORK';

export const updateNetwork = (data: boolean) => ({ type: UPDATE_NETWORK, data });
