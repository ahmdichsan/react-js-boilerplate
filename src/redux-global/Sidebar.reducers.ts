import { Action, State } from '../interfaces';
import { initialAction, initialState } from '../constants';
import { HIDE_SIDEBAR, SHOW_SIDEBAR } from './Global.actions';

export function ReducerSidebarVisibility(state: State = initialState, action: Action = initialAction) {
  switch (action.type) {
    case HIDE_SIDEBAR:
    case SHOW_SIDEBAR:
      return {
        ...state,
        action: action.type,
      };

    default:
      return state;
  }
}
