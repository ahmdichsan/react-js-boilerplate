import { Action, State } from '../interfaces';
import { initialAction, initialState } from '../constants';
import { UPDATE_NETWORK } from './Network.actions';

export function ReducerNetwork(state: State<any, boolean | null> = initialState, action: Action<boolean | null> = initialAction) {
  switch (action.type) {
    case UPDATE_NETWORK:
      return {
        ...state,
        res: action.data,
        action: action.type,
      };

    default:
      return state;
  }
}
