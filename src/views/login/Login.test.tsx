import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { render } from '@testing-library/react';
import { initialState } from '../../constants';
import Login from './Login';

const mockStore = configureMockStore();
const store = mockStore({ auth: initialState });

const Component = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Login />
    </BrowserRouter>
  </Provider>
);

describe('<Login />', () => {
  it('renders without crashing', () => {
    render(<Component />);
  });
});
