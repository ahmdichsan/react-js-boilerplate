import { call, put, takeLatest } from 'redux-saga/effects';
import { Action, Auth } from '../../interfaces';
import { loginAPI } from './Login.constants';
import { fetchLoginSuccess, fetchLoginFailed, FETCH_LOGIN } from './Login.actions';
import { HttpService } from '../../helpers';

function* workerSagaLogin(params: Action) {
  try {
    const data: object = {
      userName: params.data.username,
    };

    const headers = {
      information: params.data.password,
    };

    const response = yield call(HttpService.post, loginAPI, data, headers);

    const parseResponse: Auth = {
      id: response.data.id,
      jwt: {
        expired: response.headers.exp,
        token: response.headers.content,
      },
      name: response.data.name,
      userName: response.data.userName,
    };

    yield put(fetchLoginSuccess(parseResponse));
  } catch (error) {
    console.log(error.message);
    yield put(fetchLoginFailed(error.message));
  }
}

export const watcherSagaLogin = [takeLatest(FETCH_LOGIN, workerSagaLogin)];
