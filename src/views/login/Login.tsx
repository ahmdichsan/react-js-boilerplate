import React from 'react';
import { Container, Row } from 'reactstrap';
import { LoginForm } from './cards';
import './Login.scss';

export default function Login() {
  return (
    <Container className="center-content min-h-100vh bg-img" fluid>
      <Row className="w-40-percent">
        <LoginForm />
      </Row>
    </Container>
  );
}
