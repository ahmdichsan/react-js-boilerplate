import React from 'react';
import { DefaultRouter } from '../../interfaces';

/**
 * import this route to src > config > Routes.ts to make the path accessible from address bar
 * If you want this path accessible from side menu, go to src > config > SideBarMenu.ts and add the path
 * Further information lies in SideBarMenu.ts
 */

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes: DefaultRouter[] = [
  {
    path: '/login',
    exact: true,
    name: 'Login',
    component: React.lazy(() => import('./Login')),
  },
];

export default routes;
