import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureMockStore from 'redux-mock-store';
import { render, fireEvent, getByTestId } from '@testing-library/react';
import { initialState } from '../../../constants';
import LoginForm from './LoginForm';

const mockStore = configureMockStore();
const store = mockStore({ auth: initialState });

const Component = () => (
  <Provider store={store}>
    <BrowserRouter>
      <LoginForm />
    </BrowserRouter>
  </Provider>
);

const renderLoginForm = () => {
  return render(<Component />);
};

describe('<LoginForm />', () => {
  it('renders without crashing', () => {
    renderLoginForm();
  });

  test('should display a blank login form', async () => {
    const { findByTestId } = renderLoginForm();

    const loginForm = await findByTestId('login-form');

    expect(loginForm).toHaveFormValues({
      username: '',
      password: '',
    });
  });

  test('should allow entering a username', done => {
    const { container } = renderLoginForm();
    const inputUsername = getByTestId(container, 'username');
    fireEvent.change(inputUsername, { target: { value: 'username' } });
    expect(inputUsername.getAttribute('value')).toEqual('username');

    done();
  });

  test('should allow entering a password', done => {
    const { container } = renderLoginForm();
    const inputPassword = getByTestId(container, 'password');
    fireEvent.change(inputPassword, { target: { value: 'password' } });
    expect(inputPassword.getAttribute('value')).toEqual('password');

    done();
  });
});
