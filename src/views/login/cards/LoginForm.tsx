//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState, FormEvent, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  CardBody, Form, InputGroup, InputGroupAddon,
  InputGroupText, Input, Button, UncontrolledCollapse,
} from 'reactstrap';
//  Region Import Constants

//  Region Import Interfaces
import { HistoryProps } from '../../../interfaces';
//  Region Import Redux Action Type and Redux Action
import { fetchLogin, FETCH_LOGIN_SUCCESS } from '../Login.actions';
//  Region Import Custom Hook
import { useAuthSelector } from '../../../custom-hook';
//  Region Import Components/Cards
import { BaseCard } from '../../../cards';

import './LoginForm.scss';

function LoginForm(props: HistoryProps) {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  //  useCustomHook
  const { actionAuth, errAuth, resAuth, fetchAuth } = useAuthSelector();

  //  onHandleFunction declaration
  function onChange(event: React.ChangeEvent<HTMLInputElement>) {
    try {
      const { name, value } = event.currentTarget;

      switch (name) {
        case 'username':
          setUsername(value);
          break;

        default:
          setPassword(value);
          break;
      }
    } catch (error) {
      const errorMessage = `error onChange: ${error.message}`;
      console.log(errorMessage);
    }
  }

  async function onSubmit(event: FormEvent<HTMLFormElement | HTMLButtonElement>) {
    try {
      event.preventDefault();

      const payload = { password, username };

      dispatch(fetchLogin(payload));
    } catch (error) {
      const errorMessage = `error onSubmit: ${error.message}`;
      console.log(errorMessage);
    }
  }

  //  useEffect declaration
  useEffect(() => {
    switch (actionAuth) {
      case FETCH_LOGIN_SUCCESS:
        props.history.push('/example/dashboard');
        return;

      default:
        break;
    }

    if (resAuth) props.history.push('/example/dashboard');
  }, [actionAuth, resAuth, props.history, dispatch]);

  return (
    <BaseCard cardClassName="">
      <CardBody>
        <Form data-testid="login-form" onSubmit={onSubmit}>
          <p className="text-muted">Signs In to your account</p>
          {errAuth && (
            <p className="text-danger pb-0 mb-0">Login Failed: {errAuth}</p>
          )}
          <InputGroup className="pt-2 pb-2">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-user" />
              </InputGroupText>
            </InputGroupAddon>
            <Input
              type="text"
              placeholder="Username"
              autoComplete="username"
              name="username"
              data-testid="username"
              id="username"
              className="no-border"
              value={username}
              onChange={onChange}
            />
          </InputGroup>
          <InputGroup className="pt-2 pb-4">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-lock" />
              </InputGroupText>
            </InputGroupAddon>
            <Input
              type="password"
              placeholder="Password"
              autoComplete="current-password"
              name="password"
              data-testid="password"
              className="no-border"
              value={password}
              onChange={onChange}
            />
          </InputGroup>
          <Button className="p-0 btn-default w-100" onSubmit={onSubmit}>
            {fetchAuth && (
              <div className="is-loading"></div>
            )}
            {!fetchAuth && ('Signs In')}
          </Button>
        </Form>
        <div className="pt-3">
          <a href="/" id="toggler">
            Click here to see available user
          </a>
        </div>
        <UncontrolledCollapse toggler="#toggler">
          <div className="pt-2">
            <ul className="pl-3">
              <li>
                Username: ahmad, ichsan or baihaqi
              </li>
              <li>
                Password: 1234 (all same)
              </li>
            </ul>
          </div>
        </UncontrolledCollapse>
      </CardBody>
    </BaseCard>
  );
}

export default withRouter(LoginForm);
