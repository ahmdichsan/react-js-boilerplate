import { CustomError } from '../../../general-class';

export function sampleHelper() {
  try {
    console.log('sample');
  } catch (error) {
    throw new CustomError(error.message, 'fetchProduct');
  }
}
