import { RouteComponentProps } from "react-router-dom";

export declare interface Product {
  activeFlag: boolean;
  createdBy: string;
  createdDate: string;
  id: string;
  lastUpdatedBy: string | null;
  lastUpdatedDate: string;
  name: string;
  price: number;
  productImageName: string;
  productImageLink: string;
}

export declare interface ProductListProps extends RouteComponentProps {}
