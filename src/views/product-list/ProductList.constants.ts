export const productsAPI = 'products';
export const productsAPIById = (id: string) => `products/${id}`;
export const productBasedPath = '/example/product';
