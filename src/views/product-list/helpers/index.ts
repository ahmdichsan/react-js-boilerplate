import { CustomError } from '../../../general-class';
import { HttpService } from '../../../helpers';
import { Product } from '../interfaces';
import { productsAPI, productsAPIById } from '../ProductList.constants';

export async function fetchProduct(): Promise<Product[]> {
  try {
    const result = await HttpService.get<Product[]>(productsAPI);

    return result.data;
  } catch (error) {
    throw new CustomError(error.message, 'fetchProduct');
  }
}

export async function fetchProductById(id: string): Promise<Product> {
  try {
    const result = await HttpService.get<Product>(productsAPIById(id));

    return result.data;
  } catch (error) {
    throw new CustomError(error.message, 'fetchProductById');
  }
}
