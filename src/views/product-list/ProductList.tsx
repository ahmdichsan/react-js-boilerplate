//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Row, Col, Button, Modal } from 'reactstrap';
import { withRouter } from 'react-router-dom';

//  Region Import Constants
import { productBasedPath, productsAPIById } from './ProductList.constants';

//  Region Import Interfaces
import { Product, ProductListProps } from './interfaces';

//  Region Import Redux Action Type and Redux Action
import { showSidebar } from '../../redux-global/Global.actions';

//  Region Import Helper Function
import { fetchProduct } from './helpers';
import { HttpService } from '../../helpers';

//  Region Import Custom Hook
import { usePrevious, useNetworkSelector } from '../../custom-hook';

//  Region Import Components/Cards
import { PageContainer } from '../../components';
import { ProductItem, ProductForm } from './cards';

//  Region Import Assets


//  Region Import Style
import './ProductList.scss';


function ProductList(props: ProductListProps) {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration
  const [products, setProducts] = useState<Product[] | null>(null);
  const [submitOnProcess, setSubmitOnProcess] = useState(false);
  const prevSubmitOnProcess = usePrevious<boolean>(submitOnProcess);

  //  props object destruction

  //  useCustomHook
  const { isOnline } = useNetworkSelector();

  //  onHandleFunction declaration
  function addProduct() {
    if (submitOnProcess) return;

    props.history.push(`${productBasedPath}/add`);
  }

  function editProduct(id: string) {
    if (submitOnProcess) return;

    props.history.push(`${productBasedPath}/edit?productId=${id}`);
  }

  function onCloseModal() {
    props.history.goBack();
  }

  async function getProduct() {
    try {
      const resultFetchProduct = await fetchProduct();

      setProducts(resultFetchProduct);
    } catch (error) {
      setProducts([]);
      console.log(error.message);
    }
  }

  async function onDeleteProduct(id: string) {
    try {
      const result = await HttpService.delete(`${productsAPIById(id)}`);

      alert(result.data);

      getProduct();
    } catch (error) {
      console.log(error.message);
    }
  }

  //  useEffect declaration
  useEffect(() => {
    dispatch(showSidebar());

    if (isOnline) {
      getProduct();

      return;
    }

    setProducts([]);
  }, [dispatch, isOnline]);

  useEffect(() => {
    if (prevSubmitOnProcess && !submitOnProcess && isOnline) {
      getProduct();
    }
  }, [prevSubmitOnProcess, submitOnProcess, isOnline]);

  //  return element
  if (!isOnline) {
    return (
      <PageContainer>
        <Row className="pb-2 min-h-45vh">
          <Col md={12} lg={12} sm={12} xs={12} className="d-flex justify-content-center align-items-center text-center">
            Sorry, looks like you are offline.<br />
            This page is not accessible in offline mode.<br />
            Try to get connection if you want to access features in this page.
          </Col>
        </Row>
      </PageContainer>
    );
  }

  return (
    <PageContainer>
      <Row className="pb-2">
        <Col md={12} lg={12} sm={12} xs={12}>
          <Button color="info" onClick={addProduct}>
            Add New Product
          </Button>
        </Col>
      </Row>

      <Row>
        {!products && (
          <div className="is-loading"></div>
        )}

        {products && products.length === 0 && (
          <Col md={12} lg={12} sm={12} xs={12}>
            <div className="d-flex justify-content-center">No Data Available</div>
          </Col>
        )}

        {products && products.length > 0 && products.map((item: Product) => {
          return (
            <ProductItem
              item={item}
              key={item.id}
              onEditProduct={editProduct}
              onDeleteProduct={onDeleteProduct}
            />
          );
        })}
      </Row>

      <Modal
        isOpen={props.location.pathname.includes(`${productBasedPath}/add`) || props.location.pathname.includes(`${productBasedPath}/edit`)}
        toggle={onCloseModal}
        className="br-10px modal-md"
        contentClassName="br-10px"
        modalClassName="br-10px"
        wrapClassName="br-10px"
        unmountOnClose
      >
        <ProductForm setSubmitOnProcess={setSubmitOnProcess} />
      </Modal>
    </PageContainer>
  );
}

export default withRouter(ProductList);
