import { RouteComponentProps } from "react-router-dom";

export declare interface ProductFormProps extends RouteComponentProps<{ activity?: string }> {
  setSubmitOnProcess(submitOnProccess: boolean): void;
}
export type ActivityType = '' | 'add' | 'edit';
