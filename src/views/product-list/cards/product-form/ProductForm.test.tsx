import React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import ProductForm from '.';

const Component = () => (
  <BrowserRouter>
    <ProductForm setSubmitOnProcess={jest.fn} />
  </BrowserRouter>
);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Component />, div);
});
