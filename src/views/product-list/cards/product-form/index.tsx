//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState, useEffect } from 'react';
import { Input, Label, FormText, Button } from 'reactstrap';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import qs from 'query-string';

//  Region Import Constants
import { productsAPIById, productsAPI } from '../../ProductList.constants';

//  Region Import Interfaces
import { ProductFormProps, ActivityType } from './ProductForm.interfaces';
import { Product } from '../../interfaces';

//  Region Import Redux Action Type and Redux Action

//  Region Import Helper Function
import { fetchProductById } from '../../helpers';
import { HttpService } from '../../../../helpers';

//  Region Import Custom Hook

//  Region Import Components/Cards

//  Region Import Assets
import defaultAddImage from '../../../../assets/img/add-image.png';

//  Region Import Style
import './ProductForm.scss';

function ProductForm(props: ProductFormProps) {
  //  useDispatch declaration

  //  useState declaration
  const [activityType, setActivityType] = useState<ActivityType>('');
  const [imageSource, setImageSource] = useState('');
  const [imageFile, setImageFile] = useState<File | null>(null);
  const [submitOnProcess, setSubmitOnProcess] = useState(false);
  const [productName, setProductName] = useState('');
  const [price, setPrice] = useState('');
  const [status, setStatus] = useState('true');
  const [product, setProduct] = useState<Product | null>(null);

  //  props object destruction

  //  useCustomHook

  //  onHandleFunction declaration
  async function onChangeImage(event: React.ChangeEvent<HTMLInputElement>) {
    const fileData = event.target.files;
    if (fileData !== null && fileData[0] !== undefined) {
      const finalFile = fileData[0];
      const link = URL.createObjectURL(finalFile);

      setImageSource(link);
      setImageFile(finalFile);
    }
  }

  async function onSubmit() {
    try {
      setSubmitOnProcess(true);
      props.setSubmitOnProcess(true);

      const formData = new FormData();

      const metaData = {
        name: productName,
        price: Number(price.replace('Rp ', '').replace(/,/g, '')),
        activeFlag: status === 'true',
      };

      formData.append('product', JSON.stringify(metaData));
      if (imageFile) formData.append('productFile', imageFile);

      const querySearch = qs.parse(props.location.search) as { productId: string };

      if (activityType === 'edit' && querySearch.productId) {
        const result = await HttpService.put(`${productsAPIById(querySearch.productId)}`, formData);

        setSubmitOnProcess(false);
        props.setSubmitOnProcess(false);

        alert(result.data);

        props.history.goBack();
        return;
      }

      const result = await HttpService.post(productsAPI, formData);

      setSubmitOnProcess(false);
      props.setSubmitOnProcess(false);

      URL.revokeObjectURL(imageSource);

      alert(result.data);

      props.history.goBack();
    } catch (error) {
      console.log(error.message);
      setSubmitOnProcess(false);
      props.setSubmitOnProcess(false);
    }
  }

  async function getProductById(id: string) {
    try {
      const result = await fetchProductById(id);

      setProduct(result);
    } catch (error) {
      console.log(error.message);
    }
  }

  function onInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const inputName = event.currentTarget.name;
    const value = event.currentTarget.value;

    switch (inputName) {
      case 'name':
        setProductName(value);
        break;
      case 'price':
        setPrice(value);
        break;
      case 'status':
        setStatus(value);
        break;

      default:
        break;
    }
  }

  //  useEffect declaration
  useEffect(() => {
    const activity = props.match.params.activity as ActivityType;

    setActivityType(activity);
  }, [props.match.params.activity]);

  useEffect(() => {
    const querySearch = qs.parse(props.location.search) as { productId: string };

    if (activityType === 'edit' && querySearch.productId) {
      getProductById(querySearch.productId);
    }
  }, [activityType, props.location.search]);

  useEffect(() => {
    if (!product) return;

    setProductName(product.name || '');
    setPrice(product.price.toString() || '');
    setStatus(product.activeFlag ? 'true' : 'false');
    setImageSource(product.productImageLink || '');
  }, [product]);

  //  return element
  return (
    <>
      <div className="p-3 bgc-company-color-dark-blue company-color-yellow br-top-right-9px br-top-left-9px w-100">
        <b>{_.startCase(activityType || '')} Product</b>
      </div>
      <div className="p-3 w-100">
        <Label for="name" className="required">Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          placeholder="Put attractive and clear product name..."
          value={productName}
          onChange={onInputChange}
        />
        <Label for="price" className="pt-3 pb-0 mb-0 required">Price</Label>
        <FormText color="muted" className="pt-0 mt-0 pb-2">
          Price in IDR
        </FormText>
        <NumberFormat
          className="form-control"
          name="price"
          id="price"
          placeholder="Product price"
          thousandSeparator
          prefix="Rp "
          allowNegative={false}
          onChange={onInputChange}
          value={price}
        />
        <Label for="status" className="pt-3 required">Status</Label>
        <Input type="select" name="status" id="status" value={status} onChange={onInputChange}>
          <option value="true">Active</option>
          <option value="false">Inactive</option>
        </Input>
        <Label for="status" className="pt-3 pb-0 mb-0">Product Image</Label>
        <FormText color="muted" className="pt-0 mt-0 pb-2">
          Click the image to change it
        </FormText>
        <div>
          <Label
            for="productImage"
            role="button"
            className="modal-product-image"
          >
            <img className="modal-product-image" src={imageSource || defaultAddImage} alt={product?.id || 'product-image'} />
          </Label>
          <Input
            type="file"
            id="productImage"
            className="d-none"
            onChange={onChangeImage}
            accept=".jpg,.png,.jpeg"
          />
        </div>
        <Button className="p-0 btn-default w-100" onClick={onSubmit}>
          {submitOnProcess && (
            <div className="is-loading" />
          )}
          {!submitOnProcess && ('Submit')}
        </Button>
      </div>
    </>
  );
}

export default withRouter(ProductForm);
