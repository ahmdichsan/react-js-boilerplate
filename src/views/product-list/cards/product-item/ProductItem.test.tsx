import React from 'react';
import * as ReactDOM from 'react-dom';
import ProductItem from '.';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <ProductItem
      item={{
        activeFlag: true,
        createdBy: 'test',
        createdDate: '2020-10-10',
        id: 'id',
        lastUpdatedBy: 'test',
        lastUpdatedDate: '2020-10-10',
        name: 'name',
        price: 20000,
        productImageLink: 'link',
        productImageName: 'imageName'
      }}
      onDeleteProduct={jest.fn}
      onEditProduct={jest.fn}
    />,
    div);
});
