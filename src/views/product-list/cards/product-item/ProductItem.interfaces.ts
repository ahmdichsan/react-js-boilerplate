import { Product } from "../../interfaces";

export declare interface ProductItemProps {
  item: Product;
  onEditProduct(id: string): void;
  onDeleteProduct(id: string): void;
}
