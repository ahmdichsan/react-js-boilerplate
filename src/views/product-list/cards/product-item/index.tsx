//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

//  Region Import Constants

//  Region Import Interfaces
import { ProductItemProps } from './ProductItem.interfaces';

//  Region Import Redux Action Type and Redux Action

//  Region Import Helper Function

//  Region Import Custom Hook

//  Region Import Components/Cards
import { BaseCard } from '../../../../cards';

//  Region Import Assets
import noImage from '../../../../assets/img/no-image.png';

//  Region Import Style
import './ProductItem.scss';
import { Button } from 'reactstrap';

export default function ProductItem(props: ProductItemProps) {
  //  useDispatch declaration

  //  useState declaration
  const [isAlertOpen, setIsAlertOpen] = useState(false);

  //  props object destruction
  const { item } = props;

  //  useCustomHook

  //  onHandleFunction declaration
  function onEditProduct() {
    props.onEditProduct(item.id);
  }

  function onDeleteProduct() {
    setIsAlertOpen(true);
  }

  function confirmDelete() {
    setIsAlertOpen(false);
    props.onDeleteProduct(item.id);
  }

  //  useEffect declaration

  //  return element
  return (
    <BaseCard colSizes={{ lg: 3, md: 3, sm: 12, xs: 12 }}>
      <div className="position-relative">
        <img src={item.productImageLink || noImage} alt="product" className="product-image" />
        <Button color="info" className="edit-button" onClick={onEditProduct}>
          <i className="fa fa-pencil" />
        </Button>
        <Button color="danger" className="delete-button" onClick={onDeleteProduct}>
          <i className="fa fa-trash" />
        </Button>
      </div>
      <div className="p-3 d-flex flex-row justify-content-between">
        <div>
          {item.name}
        </div>
        <div>
          {new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            maximumFractionDigits: 0,
            minimumFractionDigits: 0,
          }).format(item.price)}
        </div>
      </div>
      <SweetAlert
        show={isAlertOpen}
        title="Attention!"
        onConfirm={confirmDelete}
        type="warning"
        confirmBtnBsStyle="danger"
        confirmBtnText="Yes"
        focusConfirmBtn={false}
        closeOnClickOutside
      >
        Are you sure to delete {item.name}?
      </SweetAlert>
    </BaseCard>
  );
}
