//  Region Import External Lib (e.g React, Reactstrap, etc)
import React from 'react';
//  Region Import Constants

//  Region Import Interfaces

//  Region Import Redux Action Type and Redux Action

//  Region Import Helper Function

//  Region Import Custom Hook

//  Region Import Components/Cards

//  Region Import Assets

//  Region Import Style
import './ExampleCard.scss';

export default function ExampleCard() {
  //  useDispatch declaration

  //  useState declaration

  //  props object destruction

  //  useCustomHook

  //  onHandleFunction declaration

  //  useEffect declaration

  //  return element
  return (
    <div>
      Example Card
    </div>
  );
}
