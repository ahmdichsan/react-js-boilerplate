//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useEffect } from 'react';
import { Row, CardHeader, CardBody } from 'reactstrap';
import { useDispatch } from 'react-redux';
import _ from 'lodash';

//  Region Import Constants


//  Region Import Interfaces


//  Region Import Redux Action Type and Redux Action
import { showSidebar } from '../../redux-global/Global.actions';

//  Region Import Helper Function
import { greetingSentence } from '../../helpers/Helpers';

//  Region Import Custom Hook
import { useAuthSelector } from '../../custom-hook';

//  Region Import Components/Cards
import { BaseCard } from '../../cards';
import { PageContainer } from '../../components';

//  Region Import Assets


//  Region Import Style
import './Dashboard.scss';

export default function Dashboard() {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration

  //  props object destruction

  //  useCustomHook
  const { resAuth } = useAuthSelector();

  //  onHandleFunction declaration

  //  useEffect declaration
  useEffect(() => {
    dispatch(showSidebar());
  }, [dispatch]);

  //  return element
  return (
    <PageContainer>
      <Row>
        <BaseCard colSizes={{ lg: 12, md: 12, sm: 12, xs: 12 }}>
          <CardHeader className="border-0">
            {greetingSentence()}, {_.startCase(resAuth?.name || '-')}
          </CardHeader>
          <CardBody>
            Welcome to ReactJS Boilerplate
          </CardBody>
        </BaseCard>
      </Row>
    </PageContainer>
  );
}
