export const toDoAPI = {
  toDoUser: 'todo/user',
  toDo: 'todo',
  markAsDone: (todoId: string) => `todo/${todoId}/toggle-status`,
};
