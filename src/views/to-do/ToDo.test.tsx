import React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../constants';
import ToDo from './ToDo';

const mockStore = configureMockStore();
const store = mockStore({ toDo: initialState });

const Component = () => (
  <Provider store={store}>
    <ToDo />
  </Provider>
);

it('renders without crashing', () => {
  const div = document.createElement('div');
  document.body.appendChild(div);
  ReactDOM.render(<Component />, div);
});
