import { Action, State } from '../../interfaces';
import { initialAction, initialState } from '../../constants';
import {
  TO_DO_FETCH, TO_DO_FETCH_SUCCESS, TO_DO_FETCH_FAILED, UPDATE_TO_DO,
  ADD_TO_DO_FETCH, ADD_TO_DO_SUCCESS, ADD_TO_DO_FAILED,
  MARK_AS_DONE_FETCH, MARK_AS_DONE_SUCCESS, MARK_AS_DONE_FAILED,
  SYNC_TO_DO_FETCH, SYNC_TO_DO_SUCCESS, SYNC_TO_DO_FAILED,
} from './ToDo.actions';
import { ToDo } from './interfaces';

export function ReducerToDo(state: State<any, ToDo[] | null> = initialState, action: Action<ToDo[] | null> = initialAction) {
  switch (action.type) {
    case TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        action: action.type,
      };

    case TO_DO_FETCH_SUCCESS:
    case TO_DO_FETCH_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    case UPDATE_TO_DO:
      return {
        ...state,
        res: action.data,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerAddToDo(state: State<ToDo | null, string | null> = initialState, action: Action<ToDo | string | null> = initialAction) {
  switch (action.type) {
    case ADD_TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data as ToDo,
        action: action.type,
      };

    case ADD_TO_DO_SUCCESS:
      return {
        ...state,
        res: action.data as string,
        fetch: false,
        action: action.type,
      };

    case ADD_TO_DO_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerMarkAsDoneToDo(state: State<ToDo | null, string | null> = initialState, action: Action<ToDo | string | null> = initialAction) {
  switch (action.type) {
    case MARK_AS_DONE_FETCH:
      return {
        ...state,
        fetch: true,
        data: action.data as ToDo,
        action: action.type,
      };

    case MARK_AS_DONE_SUCCESS:
      return {
        ...state,
        res: action.data as string,
        fetch: false,
        action: action.type,
      };

    case MARK_AS_DONE_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}

export function ReducerSyncToDo(state: State = initialState, action: Action = initialAction) {
  switch (action.type) {
    case SYNC_TO_DO_FETCH:
      return {
        ...state,
        fetch: true,
        action: action.type,
      };

    case SYNC_TO_DO_SUCCESS:
    case SYNC_TO_DO_FAILED:
      return {
        ...state,
        fetch: false,
        action: action.type,
      };

    default:
      return state;
  }
}
