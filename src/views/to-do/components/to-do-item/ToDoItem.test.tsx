import React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import ToDoItem from '.';

const mockStore = configureMockStore();
const store = mockStore({});

const Component = () => (
  <Provider store={store}>
    <BrowserRouter>
      <ToDoItem toDo={null} />
    </BrowserRouter>
  </Provider>
);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Component />, div);
});
