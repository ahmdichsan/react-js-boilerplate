import { ToDo } from "../../interfaces";

export declare interface ToDoItemProps {
  toDo: ToDo | null;
}
