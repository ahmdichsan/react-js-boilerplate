//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Row, Button, UncontrolledTooltip } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

//  Region Import Constants

//  Region Import Interfaces
import { ToDoItemProps } from './ToDoItem.interfaces';

//  Region Import Redux Action Type and Redux Action
import { markAsDoneFetch, toDoFetch } from '../../ToDo.actions';

//  Region Import Helper Function

//  Region Import Custom Hook
import { useMarkAsDoneToDoSelector, useNetworkSelector } from '../../../../custom-hook';

//  Region Import Components/Cards
import { BaseCard } from '../../../../cards';
import { HttpService } from '../../../../helpers';

//  Region Import Assets


//  Region Import Style

export default function ToDoItem(props: ToDoItemProps) {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration
  const [isDeleteDisabled, setIsDeleteDisabled] = useState(false);

  //  props object destruction
  const { toDo } = props;

  //  useCustomHook
  const { fetchMarkAsDoneToDo } = useMarkAsDoneToDoSelector();
  const { isOnline } = useNetworkSelector();

  //  onHandleFunction declaration
  function markAsDone() {
    try {
      if (!toDo) return;

      dispatch(markAsDoneFetch(toDo));
    } catch (error) {
      const errorMessage = `error markAsDone: ${error.message}`;
      console.log(errorMessage);
    }
  }

  async function deleteToDo() {
    try {
      if (!props.toDo) return;

      setIsDeleteDisabled(true);

      await HttpService.delete(`todo/${props.toDo.id}`);

      setIsDeleteDisabled(false);

      dispatch(toDoFetch());
    } catch (error) {
      const errorMessage = `error deleteToDo: ${error.message}`;
      console.log(errorMessage);
    }
  }

  //  useEffect declaration

  //  return element
  if (!toDo) return null;

  return (
    <Row>
      <BaseCard cardClassName="p-2 mb-3">
        <div className="d-flex flex-row justify-content-between align-items-center">
          <div>
            {toDo.toDo} {!toDo.isSynced && <span className="text-muted">[Not Synced]</span>}
          </div>
          {!toDo.isToDoDone && (
            <Button
              onClick={markAsDone}
              color={!fetchMarkAsDoneToDo ? "success" : "secondary"}
              disabled={!!fetchMarkAsDoneToDo}
            >
              Mark as Done
            </Button>
          )}
          {toDo.isToDoDone && (
            <>
              <Button
                onClick={deleteToDo}
                color={isOnline || !isDeleteDisabled ? "danger" : "secondary"}
                disabled={!isOnline || isDeleteDisabled}
                id="Delete"
              >
                <FontAwesomeIcon icon={faTrash} size="sm" />
              </Button>
              <UncontrolledTooltip target="Delete" delay={{ show: 1000, hide: 10 }} placement="left">
                <div className="font-size-12">
                  This feature currently available only in online mode
                </div>
              </UncontrolledTooltip>
            </>
          )}
        </div>
      </BaseCard>
    </Row>
  );
}
