import { ToDo } from "./interfaces";

/**
 * fetch todo
 */
export const TO_DO_FETCH = 'TO_DO_FETCH';
export const toDoFetch = () => ({ type: TO_DO_FETCH });

export const TO_DO_FETCH_SUCCESS = 'TO_DO_FETCH_SUCCESS';
export const toDoFetchSuccess = () => ({ type: TO_DO_FETCH_SUCCESS });

export const TO_DO_FETCH_FAILED = 'TO_DO_FETCH_FAILED';
export const toDoFetchFailed = () => ({ type: TO_DO_FETCH_FAILED });

export const UPDATE_TO_DO = 'UPDATE_TO_DO';
export const updateToDo = (data: ToDo[]) => ({ type: UPDATE_TO_DO, data });

/**
 * add to do
 */
export const ADD_TO_DO_FETCH = 'ADD_TO_DO_FETCH';
export const addToDoFetch = (data: string) => ({ type: ADD_TO_DO_FETCH, data });

export const ADD_TO_DO_SUCCESS = 'ADD_TO_DO_SUCCESS';
export const addToDoSuccess = () => ({ type: ADD_TO_DO_SUCCESS });

export const ADD_TO_DO_FAILED = 'ADD_TO_DO_FAILED';
export const addToDoFailed = () => ({ type: ADD_TO_DO_FAILED });

/**
 * mark as done
 */
export const MARK_AS_DONE_FETCH = 'MARK_AS_DONE_FETCH';
export const markAsDoneFetch = (data: ToDo) => ({ type: MARK_AS_DONE_FETCH, data });

export const MARK_AS_DONE_SUCCESS = 'MARK_AS_DONE_SUCCESS';
export const markAsDoneSuccess = () => ({ type: MARK_AS_DONE_SUCCESS });

export const MARK_AS_DONE_FAILED = 'MARK_AS_DONE_FAILED';
export const markAsDoneFailed = () => ({ type: MARK_AS_DONE_FAILED });

/**
 * sync todo
 */
export const SYNC_TO_DO_FETCH = 'SYNC_TO_DO_FETCH';
export const syncToDoFetch = () => ({ type: SYNC_TO_DO_FETCH });

export const SYNC_TO_DO_SUCCESS = 'SYNC_TO_DO_SUCCESS';
export const syncToDoSuccess = () => ({ type: SYNC_TO_DO_SUCCESS });

export const SYNC_TO_DO_FAILED = 'SYNC_TO_DO_FAILED';
export const syncToDoFailed = () => ({ type: SYNC_TO_DO_FAILED });


/**
 * ambil baru cuma pas reload atau kunjungin ulang halaman todo
 * 
 * add
 * 1. dispatch => isToDoDone auto false
 *    1. kalo gagal/offline => not synced => store
 *    2. kalo berhasil => synced => store
 * 
 * mark as done
 * 1. dispatch => isToDoDone auto true
 *    1. kalo gagal/offline => not synced => store
 *    2. kalo berhasil => synced => store
 * 
 * saat online sblmnya offline
 * 1. dispatch syncedAction
 *    1. ambil semua data todo di redux
 *    2. cari yg belom ke sync aja
 *    3. looping call api satu2, tiap satu selesai, update data nya di local => klo tiba2 gagal, masukin ke store yg udh berhasil
 *    4. kalo ada yg gagal, ya masukin yg udh berhasil
 *    5. ada yg hit api create kalo isToDoDone false, kalo true, hit api mark as done
 */