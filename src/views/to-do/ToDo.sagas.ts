import { call, put, takeLatest, select } from 'redux-saga/effects';
import moment from 'moment';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import {
  TO_DO_FETCH, toDoFetchSuccess, toDoFetchFailed, updateToDo,
  ADD_TO_DO_FETCH, addToDoSuccess, addToDoFailed,
  MARK_AS_DONE_FETCH, markAsDoneSuccess, markAsDoneFailed,
  SYNC_TO_DO_FETCH, syncToDoSuccess, syncToDoFailed, toDoFetch,
} from './ToDo.actions';
import { HttpService, getNetworkState, getToDoState } from '../../helpers';
import { toDoAPI } from './ToDo.constants';
import { ToDo } from './interfaces';
import { Action } from '../../interfaces';

function* workerSagaTodo() {
  try {
    const isOnline: boolean | null = yield select(getNetworkState);

    if (!isOnline) {
      yield put(toDoFetchSuccess());
      return;
    }

    const response = yield call(HttpService.get, toDoAPI.toDoUser);

    const data = response.data as ToDo[];

    const finalData = data.map((item: ToDo) => {
      return {
        ...item,
        isSynced: true,
      };
    });

    yield put(updateToDo(finalData));

    yield put(toDoFetchSuccess());
  } catch (error) {
    const errorMessage = `error workerSagaTodo: ${error.message}`;
    console.log(errorMessage);
    yield put(toDoFetchFailed());
  }
}

function* workerSagaAddTodo(params: Action<string>) {
  const data: ToDo = {
    id: uuidv4(),
    toDo: params.data,
    isSynced: false,
    isToDoDone: false,
    createdDate: moment(new Date()).format(),
    createdBy: '',
    lastUpdatedBy: '',
    lastUpdatedDate: null,
  };

  const currentToDo: ToDo[] | null = yield select(getToDoState);

  try {
    const isOnline: boolean | null = yield select(getNetworkState);

    if (!isOnline) throw new Error('Not Connected');

    const result = yield call(HttpService.post, toDoAPI.toDo, data);

    const toDoStoredInDb = result.data as ToDo;
    const finalData: ToDo[] = currentToDo ? [...currentToDo, { ...toDoStoredInDb, isSynced: true }] : [{ ...toDoStoredInDb, isSynced: true }];

    yield put(updateToDo(finalData));

    yield put(addToDoSuccess());
  } catch (error) {
    const errorMessage = `error workerSagaAddTodo: ${error.message}`;
    console.log(errorMessage);

    const finalData: ToDo[] = currentToDo ? [...currentToDo, { ...data }] : [{ ...data }];

    yield put(updateToDo(finalData));

    yield put(addToDoFailed());
  }
}

function* workerSagaMarkAsDone(params: Action<ToDo>) {
  const data: ToDo = {
    ...params.data,
    isSynced: false,
    isToDoDone: true,
    lastUpdatedDate: moment(new Date()).format(),
  };

  const currentToDo: ToDo[] = yield select(getToDoState) || [];

  try {
    const isOnline: boolean | null = yield select(getNetworkState);

    if (!isOnline) throw new Error('Not Connected');

    yield call(HttpService.put, toDoAPI.markAsDone(data.id), data);
    
    const indexSelectedToDo = currentToDo.findIndex(item => item.id === data.id);

    currentToDo[indexSelectedToDo].isSynced = true;
    currentToDo[indexSelectedToDo].isToDoDone = true;
    
    const finalData: ToDo[] = _.cloneDeep(currentToDo);

    yield put(updateToDo(finalData));

    yield put(markAsDoneSuccess());
  } catch (error) {
    const errorMessage = `error workerSagaMarkAsDone: ${error.message}`;
    console.log(errorMessage);

    const indexSelectedToDo = currentToDo.findIndex(item => item.id === data.id);

    currentToDo[indexSelectedToDo].isSynced = false;
    currentToDo[indexSelectedToDo].isToDoDone = true;
    
    const finalData: ToDo[] = _.cloneDeep(currentToDo);

    yield put(updateToDo(finalData));

    yield put(markAsDoneFailed());
  }
}

function* workerSagaSyncToDo() {
  const currentToDo: ToDo[] | null = yield select(getToDoState);

  try {
    if (!currentToDo || (currentToDo && currentToDo.length === 0)) {
      yield put(syncToDoSuccess());
      return;
    }

    const notSyncedToDo: ToDo[] = currentToDo.filter((item: ToDo) => !item.isSynced);
    const syncedToDo: ToDo[] = currentToDo.filter((item: ToDo) => item.isSynced);

    if (notSyncedToDo.length === 0) {
      yield put(syncToDoSuccess());
      return;
    }

    const isOnline: boolean | null = yield select(getNetworkState);

    if (!isOnline) throw new Error('Not Connected');

    try {
      for (let i = 0; i < notSyncedToDo.length; i += 1) {
        const item = notSyncedToDo[i];

        if (item.isToDoDone) {
          // mark as done
          yield call(HttpService.put, toDoAPI.markAsDone(item.id), item);

          item.isSynced = true;
          continue;
        }

        // add to do
        yield call(HttpService.post, toDoAPI.toDo, item);

        item.isSynced = true;
      }

      yield put(toDoFetch());
    } catch (error) {
      const errorMessage = `error try loop workerSagaSyncToDo: ${error.message}`;
      console.log(errorMessage);

      // update local
      const updatedData: ToDo[] = [...syncedToDo, ...notSyncedToDo];
      const finalData = _.orderBy(updatedData, { createdDate: 'DESC' });

      yield put(updateToDo(finalData));
    }

    yield put(syncToDoSuccess());
  } catch (error) {
    const errorMessage = `error workerSagaSyncToDo: ${error.message}`;
    console.log(errorMessage);

    yield put(syncToDoFailed());
  }
}

export const watcherSagaToDo = [
  takeLatest(TO_DO_FETCH, workerSagaTodo),
  takeLatest(ADD_TO_DO_FETCH, workerSagaAddTodo),
  takeLatest(MARK_AS_DONE_FETCH, workerSagaMarkAsDone),
  takeLatest(SYNC_TO_DO_FETCH, workerSagaSyncToDo),
];
