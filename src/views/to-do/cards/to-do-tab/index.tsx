//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import { useDispatch } from 'react-redux';

//  Region Import Constants

//  Region Import Interfaces
import { ToDo } from '../../interfaces';

//  Region Import Redux Action Type and Redux Action
import { toDoFetch } from '../../ToDo.actions';

//  Region Import Helper Function

//  Region Import Custom Hook
import { useToDoSelector } from '../../../../custom-hook';

//  Region Import Components/Cards
import { BaseCard } from '../../../../cards';
import { ToDoItem } from '../../components';

//  Region Import Assets

//  Region Import Style
import './ToDoTab.scss';

export default function ToDoTab() {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration
  const [activeTab, setActiveTab] = useState('1');
  const [unDoneToDo, setUnDoneToDo] = useState<ToDo[]>([]);
  const [doneToDo, setDoneToDo] = useState<ToDo[]>([]);

  //  props object destruction

  //  useCustomHook
  const { resToDo, fetchToDo } = useToDoSelector();

  //  onHandleFunction declaration
  function toggle(tab: string) {
    if (activeTab !== tab) setActiveTab(tab);
  }

  //  useEffect declaration
  useEffect(() => {
    dispatch(toDoFetch());
  }, [dispatch]);

  useEffect(() => {
    if (!resToDo) return;

    const resultUnDoneToDo = resToDo.filter(item => !item.isToDoDone);
    const resultDoneToDo = resToDo.filter(item => item.isToDoDone);

    setUnDoneToDo(resultUnDoneToDo);
    setDoneToDo(resultDoneToDo);
  }, [resToDo]);

  //  return element
  return (
    <BaseCard cardColClassName="mt-3" cardClassName="min-h-45vh">
      <div className="p-3">
        <Nav tabs>
          <NavItem className="cursor-pointer">
            <NavLink
              to="#"
              className={classnames({ active: activeTab === '1' }, 'custom-tab')}
              onClick={() => { toggle('1'); }}
            >
              To Do
            </NavLink>
          </NavItem>
          <NavItem className="cursor-pointer">
            <NavLink
              to="#"
              className={classnames({ active: activeTab === '2' }, 'custom-tab')}
              onClick={() => { toggle('2'); }}
            >
              Done
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab} className="mt-3">
          <TabPane tabId="1">
            {activeTab === '1' && !fetchToDo && unDoneToDo.length === 0 && (
              <div>
                You have nothing in your To Do. Make one!
              </div>
            )}
            {activeTab === '1' && unDoneToDo.map((item: ToDo) => <ToDoItem toDo={item} key={item.id} />)}
          </TabPane>
          <TabPane tabId="2">
            {activeTab === '2' && !fetchToDo && doneToDo.length === 0 && (
              <div>
                Nothings done yet. Lets get all task in To Do done
              </div>
            )}
            {activeTab === '2' && doneToDo.map((item: ToDo) => <ToDoItem toDo={item} key={item.id} />)}
          </TabPane>
        </TabContent>
      </div>
    </BaseCard>
  );
}
