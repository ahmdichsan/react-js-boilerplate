import React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { initialState } from '../../../../constants';
import ToDoTab from '.';

const mockStore = configureMockStore();
const store = mockStore({ toDo: initialState });

const Component = () => (
  <Provider store={store}>
    <ToDoTab />
  </Provider>
);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Component />, div);
});
