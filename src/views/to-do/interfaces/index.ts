export declare interface ToDo {
  createdBy: string;
  createdDate: string;
  lastUpdatedBy: string | null;
  lastUpdatedDate: string | null;
  activeFlag?: boolean;
  id: string;
  toDo: string;
  isToDoDone: boolean;
  isSynced: boolean;
}
