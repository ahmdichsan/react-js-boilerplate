//  Region Import External Lib (e.g React, Reactstrap, etc)
import React, { useState, useEffect } from 'react';
import { Row, Col, Label, Input, Button, UncontrolledTooltip, Form } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';

//  Region Import Constants


//  Region Import Interfaces


//  Region Import Redux Action Type and Redux Action
import { showSidebar } from '../../redux-global/Global.actions';
import { addToDoFetch } from './ToDo.actions';

//  Region Import Helper Function


//  Region Import Custom Hook
import { useAddToDoSelector } from '../../custom-hook';

//  Region Import Components/Cards
import { PageContainer } from '../../components';
import { ToDoTab } from './cards';

//  Region Import Assets

//  Region Import Style
import './ToDo.scss';

export default function ToDo() {
  //  useDispatch declaration
  const dispatch = useDispatch();

  //  useState declaration
  const [toDo, setToDo] = useState('');

  //  props object destruction

  //  useCustomHook
  const { fetchAddToDo } = useAddToDoSelector();

  //  onHandleFunction declaration
  function onChangeToDo(event: React.ChangeEvent<HTMLInputElement>) {
    setToDo(event.target.value);
  }

  function onSubmit(event: React.FormEvent<HTMLFormElement | HTMLButtonElement>) {
    try {
      event.preventDefault();

      dispatch(addToDoFetch(toDo));
    } catch (error) {
      const errorMessage = `error onSubmit: ${error.message}`;
      console.log(errorMessage);
    }
  }

  //  useEffect declaration
  useEffect(() => {
    dispatch(showSidebar());
  }, [dispatch]);

  //  return element
  return (
    <PageContainer>
      <Row className="pb-2">
        <Col md={12} lg={12} sm={12} xs={12}>
          <Label>Add To Do</Label>&nbsp;
          <FontAwesomeIcon icon={faInfoCircle} size="sm" id="info" />
          <UncontrolledTooltip target="#info" placement="right" delay={{ show: 1000, hide: 0 }} >
            This page is accessible in offline mode.
            You can create or mark as done a todo in offline mode.
            All data will be synced between your local and the server when you back online.
            You can check the status by looking at the “Not Synced” flag.
          </UncontrolledTooltip>
        </Col>
      </Row>

      <Row className="pb-2">
        <Col md={12} lg={12} sm={12} xs={12}>
          <Form data-testid="login-form" onSubmit={onSubmit}>
            <Row>
              <Col md={11} lg={11} sm={12} xs={12}>
                <Input
                  type="text"
                  name="todo"
                  id="todo"
                  placeholder="Write your todo here"
                  value={toDo}
                  onChange={onChangeToDo}
                />
              </Col>
              <Col md={1} lg={1} sm={12} xs={12}>
                <Button className="w-100" color={!fetchAddToDo ? "info" : "secondary"} onSubmit={onSubmit} disabled={!!fetchAddToDo}>
                  Add
                </Button>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>

      <Row className="pb-2">
        <ToDoTab />
      </Row>
    </PageContainer>
  );
}
