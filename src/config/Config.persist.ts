import storage from 'redux-persist/lib/storage';
import createEncryptor from 'redux-persist-transform-encrypt';
import { createBlacklistFilter } from 'redux-persist-transform-filter';
import 'dotenv/config';

const encryptor = createEncryptor({
  secretKey: 'put-secret-for-redux-persist-here',
  onError: error => {
    console.log('createEncryptor error ', error);
  },
});

const saveAuthSubsetBlacklistFilter = createBlacklistFilter('auth', ['data', 'err']);

const configPersist: any = {
  key: 'root',
  storage,
  transforms: [saveAuthSubsetBlacklistFilter, encryptor],
};

export default configPersist;
