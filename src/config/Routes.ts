import { DefaultRouter } from '../interfaces';

import login from '../views/login/Login.router';
import dashboard from '../views/dashboard/Dashboard.router';
import product from '../views/product-list/ProductList.router';
import toDo from '../views/to-do/ToDo.router';


/**
 * if your new route need container (TopBar, SideBar and Footer), put in RouterContainer
 * else, put in RouterNonContainer
 */

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
export const routerContainer: DefaultRouter[] = [
  ...dashboard,
  ...product,
  ...toDo,
];

export const routerNonContainer: DefaultRouter[] = [
  ...login,
];
