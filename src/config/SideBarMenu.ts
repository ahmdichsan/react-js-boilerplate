import timelineIcon from '../assets/icon/timeline.svg';
import { SideMenuWithChild } from '../interfaces';

/**
 * Example
 * const sideMenu: SidemenuWithChild[] = [
 *  {
 *    menu: 'Menu One',
 *    path: '/menu-one',
 *    icon: menuOneIcon,
 *    accessRole: 'roleMenuOne',
 *  },
 *  {
 *    menu: 'Menu Two',
 *    path: '/', // just fill with slash. won't use it anyway.
 *    icon: menuTwoIcon,
 *    accessRole: 'roleMenuTwo',
 *    children: [
 *      {
 *        menu: 'Menu Two Child',
 *        path: '/menu-two/child',
 *        icon: menuTwoChildIcon,
 *        accessRole: 'roleMenuTwoChild',
 *      },
 *    ]
 *  },
 * ];
 */

const sideMenu: SideMenuWithChild[] = [
  {
    menu: 'Dashboard',
    path: '/example/dashboard',
    icon: timelineIcon,
    accessRole: '',
  },
  {
    menu: 'Product',
    path: '/example/product',
    icon: timelineIcon,
    accessRole: '',
  },
  {
    menu: 'My To Do',
    path: '/example/todo',
    icon: timelineIcon,
    accessRole: '',
  },
];

export default sideMenu;
