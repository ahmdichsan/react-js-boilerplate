/*
 * Examples:
 * import { watcherModules } from '../modules/some/SagaSome';
 * export default [
 *   ...watcherModules,
 * ];
 */

import { watcherSagaLogin } from '../views/login/Login.sagas';
import { watcherSagaToDo } from '../views/to-do/ToDo.sagas';

const configSaga = [...watcherSagaLogin, ...watcherSagaToDo];

export default configSaga;
