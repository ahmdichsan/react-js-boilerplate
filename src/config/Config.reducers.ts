/*
 * Examples:
 * import { ReducerSome } from '../modules/auth/ReducerSome';
 * const reducers: object = {
 *   some: ReducerSome,
 * };
 * export default reducers;
 */

import { ReducerAuth } from '../views/login/Login.reducers';
import { ReducerAddToDo, ReducerMarkAsDoneToDo, ReducerSyncToDo, ReducerToDo } from '../views/to-do/ToDo.reducers';
import { ReducerSidebarVisibility, ReducerNetwork } from '../redux-global/Global.reducers';

const configReducer = {
  auth: ReducerAuth,
  sidebarVisibility: ReducerSidebarVisibility,
  network: ReducerNetwork,
  toDo: ReducerToDo,
  addToDo: ReducerAddToDo,
  markAsDoneToDo: ReducerMarkAsDoneToDo,
  syncToDo: ReducerSyncToDo,
};

export default configReducer;
