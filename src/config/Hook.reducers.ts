import storage from 'redux-persist/lib/storage';
import { USER_LOG_OUT } from '../redux-global/Global.actions';

const hookReducer: any = (state: any, action: any) => {
  // do your hook here
  if (action.type === USER_LOG_OUT) {
    storage.removeItem('persist:root');
    state = undefined;
  }

  return { state, action };
};

export default hookReducer;
