import _ from 'lodash';
import { ToDo } from '../views/to-do/interfaces';

export function convertToSentenceText(value: string): string {
  return _.startCase(_.toLower(value));
}

export function greetingSentence(): string {
  const hour = new Date().getHours();

  if (hour >= 0 && hour < 12) return "Good morning";

  if (hour >= 12 && hour < 17) return "Good afternoon";

  return "Good evening";
}

export async function objectURLToFile(objectURL: string, fileName: string, fileType: string): Promise<File> {
  return await fetch(objectURL)
    .then(r => r.blob())
    .then(blobFile => new File([blobFile], fileName, { type: fileType }));
}

export function getToDoState(state: any): ToDo[] | null {
  return state?.toDo?.res as ToDo[];
}

export function getNetworkState(state: any): boolean | null {
  return state?.network?.res as boolean;
}
