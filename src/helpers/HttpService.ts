/**
 * This is HttpService, function that wrap axios package
 * With this function, caller of HttpService does not have to provide token
 * since it is already called here by calling the Store
 */

import axios, { AxiosRequestConfig, Method, AxiosResponse } from 'axios';
import 'dotenv/config';
import _ from 'lodash';
import { store } from '../app/App.store';
import { Auth } from '../interfaces';

const resTimeout = 'Server is taking too long to respond. This can be caused by poor connectivity or an error in our server. Please try again later';

const getRequestOptions = (method: Method, data: any, headers?: object | null, reqOptions?: AxiosRequestConfig) => {
  const getReduxState: any = store.getState();

  let parseHeaders = {
    'Content-Type': 'application/json',
    Authorization: '',
  };

  const auth: Auth | null = getReduxState?.auth?.res;

  if (auth?.jwt?.token) parseHeaders.Authorization = `Bearer ${auth.jwt.token}`;

  if (headers) parseHeaders = { ..._.cloneDeep(parseHeaders), ..._.cloneDeep(headers) };

  let requestOptions: AxiosRequestConfig = {
    method,
    headers: parseHeaders,
    data,
    timeout: 60000,
  };

  if (reqOptions) requestOptions = { ...requestOptions, ...reqOptions };

  return requestOptions;
};

const doFetch = async (requestOptions: AxiosRequestConfig, url: string) => {
  const API_HOST: string = process.env.REACT_APP_API_HOST || 'localhost';
  const API_PORT: number = Number(process.env.REACT_APP_API_PORT) || 8080;
  const API_PROTOCOL: string = process.env.REACT_APP_API_PROTOCOL || 'http';

  let path = `${API_PROTOCOL}://${API_HOST}:${API_PORT}/${url}`;
  path = API_HOST.includes('https://') || API_HOST.includes('http://') ? `${API_HOST}/${url}` : path;
  path = url.includes('https://') || url.includes('http://') ? url : path;

  return axios(path, requestOptions)
    .then()
    .catch(err => {
      // here all status < 200 && status >= 300 handled
      // here is info about http status code: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
      let errorMessage = err.response && err.response.data ? err.response.data : err.message;
      errorMessage = errorMessage || JSON.parse(JSON.stringify(err));

      // custom message if request timeout. In order to make the message more user friendly
      if (err.code === 'ECONNABORTED') errorMessage = resTimeout;

      throw new Error(errorMessage);
    });
};

/**
 * Usage:
 *
 * const functionName = async () => {
 *    const headers = {
 *      // put your additional config headers here,
 *    };
 *
 *    const result = await HttpService.get<DataInterface>('api/url', headers);
 *    console.log(result);
 * }
 *
 */

export const HttpService = {
  get<T = any>(url: string, headers: object | null = null, reqOptions?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const requestOptions = getRequestOptions('GET', null, headers, reqOptions);
    return doFetch(requestOptions, url);
  },
  post<T = any>(url: string, data: any, headers: object | null = null, reqOptions?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const requestOptions = getRequestOptions('POST', data, headers, reqOptions);
    return doFetch(requestOptions, url);
  },
  put<T = any>(url: string, data: any, headers: object | null = null, reqOptions?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const requestOptions = getRequestOptions('PUT', data, headers, reqOptions);
    return doFetch(requestOptions, url);
  },
  delete<T = any>(url: string, headers: object | null = null, reqOptions?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    const requestOptions = getRequestOptions('DELETE', null, headers, reqOptions);
    return doFetch(requestOptions, url);
  },
};
