// run install devDependencies here
const exec = require('child_process').exec;

const devDependencies = {
    "@types/bootstrap": "^4.5.0",
    "@types/classnames": "^2.2.10",
    "@types/enzyme": "^3.10.5",
    "@types/enzyme-adapter-react-16": "^1.0.6",
    "@types/enzyme-to-json": "^1.5.3",
    "@types/jest": "26.0.9",
    "@types/lodash": "^4.14.159",
    "@types/node": "14.0.27",
    "@types/node-sass": "^4.11.1",
    "@types/prop-types": "^15.7.3",
    "@types/react": "16.9.45",
    "@types/react-dom": "16.9.8",
    "@types/react-redux": "^7.1.9",
    "@types/react-router-config": "^5.0.1",
    "@types/react-router-dom": "^5.1.5",
    "@types/reactstrap": "^8.5.0",
    "@types/redux-mock-store": "^1.0.2",
    "@types/redux-persist-transform-encrypt": "^2.0.1",
    "@types/redux-persist-transform-filter": "0.0.4",
    "@types/uuid": "^8.0.1",
    "eslint-plugin-react-hooks": "^4.0.8",
    "jest-environment-jsdom-sixteen": "^1.0.3"
};

function installDevDependencies() {
    try {
        let command = `npm i --save-dev `;
        const listDevDependencies = Object.keys(devDependencies).map((value) => `${value}@${devDependencies[value]}`).join(' ');
        command += `${listDevDependencies}`;
        
        exec(command, error => {
            if (error) console.log(`error exec: ${error.message}`);
        });
    } catch (error) {
        const errorMessage = `error installDevDependencies: ${error.message}`;
        console.log(errorMessage);
    }
}

installDevDependencies();
