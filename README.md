This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Click [here](https://react-js-boilerplate.herokuapp.com/login) to see the demo app.

## Table of Contents

1.  [Clone and Installation Guide](#clone-and-installation-guide)
2.  [Installation through `npm`](#installation-through-npm)
3.  [Available Scripts](#available-scripts)
    1. [`npm start`](#npm-start)
    2. [`npm run install-dev-dependencies`](#npm-run-install-dev-dependencies)
    3. [`npm test`](#npm-test)
    4. [`npm run coverage`](#npm-run-coverage)
    5. [`npm run build`](#npm-run-build)
    6. [`npm run eject`](#npm-run-eject)
4.  [Learn More React](#learn-more-react)
5.  [Boilerplate Description](#boilerplate-description)
    1. [Try to Use Hook](#try-to-use-hook)
    2. [Rules of Hook](#rules-of-hook)
    3. [Coding Guidline References](#coding-guidline-references)
    4. [Folder Structure](#folder-structure)
6. [Boilerplate Built-in Feature and Configuration](#boilerplate-built-in-feature-and-configuration)
7. [Boilerplate Convention](#boilerplate-convention)
    1. [Importing Order inside Component](#importing-order-inside-component)
    2. [Code Region inside Component](#code-region-inside-component)
8. [How to Add New Views](#how-to-add-new-views)
9. [How to Add New Shared Cards](#how-to-add-new-shared-cards)
10. [How to Add New Shared Components](#how-to-add-new-shared-components)
11. [How to Add Custom Style](#how-to-add-custom-style)

## Clone and Installation Guide

1. git clone
2. npm install
3. create file `.env` in root project
4. copy all environment variables inside `.env.example` and paste into `.env`
5. change the value inside `.env` with appropriate value
6. npm start
7. happy coding!
8. Ah, available users for Sign In displayed in the login screen

## Installation through `npm`

**Note: currently this is not updated yet. We will update it soon. So, clone the repo instead.**

1. run this command: `npx create-react-app my-app --template ecom-cra-ts`
2. run this command to install `devDependencies`: `npm run install-dev-dependencies` - This step required since CRA blacklisting `devDependencies` to be installed at initial project creation
2. create file `.env` in root project
3. copy all environment variables inside `.env.example` and paste into `.env`
4. change the value inside `.env` with appropriate value
5. npm start
6. happy coding!
7. Ah, available users for Sign In displayed in the login screen

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console (if any).

### `npm run install-dev-dependencies`

Runs installation `devDependencies` used in this boilerplate. You will not use this if you are cloning this boilerplate from the repo. This is used for those who use this boilerplate by installation via `npm`.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run coverage`

Launches the test runner and give coverage result at the end of the test.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More React

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Boilerplate Description

### Try to use Hook

Since React Hooks are stable in React 16.8, this boilerplate is created with React Hook. We also encourage developers who use this boilerplate to give a try for React Hooks. Try it [here](https://reactjs.org/docs/hooks-intro.html).

### Rules of Hook

https://reactjs.org/docs/hooks-rules.html#explanation

### Coding Guidline References

1. https://github.com/airbnb/javascript/tree/master/react
2. https://github.com/typescript-cheatsheets/react-typescript-cheatsheet

### Folder Structure
```
├── .vscode
├── bin
├── node_modules
├── public
├── src
│   ├── app - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - # Root Application
│   │   ├── App.reducers.ts
│   │   ├── App.sagas.ts
│   │   ├── App.store.ts
│   │   ├── App.test.tsx
│   │   └── App.tsx
│   ├── assets
│   │   ├── img
│   │   └── icon
│   ├── cards - - - - - - - - - - - - - - - - - - - - - - - - - - - - - # Shared Cards as Services
│   │   ├── your-shared-card-folder
│   │   │   ├── index.tsx
│   │   │   ├── YourSharedCard.actions.ts
│   │   │   ├── YourSharedCard.constants.ts
│   │   │   ├── YourSharedCard.interfaces.ts
│   │   │   ├── YourSharedCard.reducers.ts
│   │   │   ├── YourSharedCard.sagas.ts
│   │   │   ├── YourSharedCard.scss
│   │   │   └── YourSharedCard.test.tsx
│   │   └── index.ts
│   ├── components - - - - - - - - - - - - - - - - - - - - - - - - - -  # Shared Components
│   │   ├── your-shared-component-folder
│   │   │   ├── index.tsx
│   │   │   ├── YourSharedComponent.actions.ts
│   │   │   ├── YourSharedComponent.constants.ts
│   │   │   ├── YourSharedComponent.interfaces.ts
│   │   │   ├── YourSharedComponent.reducers.ts
│   │   │   ├── YourSharedComponent.sagas.ts
│   │   │   ├── YourSharedComponent.scss
│   │   │   └── YourSharedComponent.test.tsx
│   │   └── index.ts
│   ├── config
│   │   ├── Config.persist.ts
│   │   ├── Config.reducers.ts
│   │   ├── Config.sagas.ts
│   │   ├── Hook.reducers.ts
│   │   ├── index.ts
│   │   ├── Routes.ts
│   │   └── SideBarMenu.ts
│   ├── constants
│   │   └── index.ts
│   ├── custom-hook
│   │   ├── index.ts
│   │   └── YourCustomHook.ts
│   ├── general-class
│   │   ├── index.ts
│   │   └── YourGeneralClass.ts
│   ├── helpers
│   │   ├── YourHelpers.ts
│   │   └── index.ts
│   ├── interfaces
│   │   └── index.ts
│   ├── redux-global
│   │   ├── Global.action.ts
│   │   └── Global.reducers.ts
│   ├── scss
│   │   ├── Animate.scss
│   │   ├── Custom.scss
│   │   ├── index.scss
│   │   └── Variables.scss
│   ├── views - - - - - - - - - - - - - - - - - - # Views represents folders of Page in the App
│   │   ├── your-views-folder
│   │   │   ├── cards
│   │   │   │   ├── your-views-card
│   │   │   │   │   ├── index.tsx
│   │   │   │   │   ├── YourViewsCard.actions.ts
│   │   │   │   │   ├── YourViewsCard.constants.ts
│   │   │   │   │   ├── YourViewsCard.interfaces.ts
│   │   │   │   │   ├── YourViewsCard.reducers.ts
│   │   │   │   │   ├── YourViewsCard.sagas.ts
│   │   │   │   │   ├── YourViewsCard.scss
│   │   │   │   │   └── YourViewsCard.test.tsx
│   │   │   │   └── index.ts
│   │   │   ├── components
│   │   │   │   ├── your-views-component
│   │   │   │   │   ├── index.tsx
│   │   │   │   │   ├── YourViewsComponent.actions.ts
│   │   │   │   │   ├── YourViewsComponent.constants.ts
│   │   │   │   │   ├── YourViewsComponent.interfaces.ts
│   │   │   │   │   ├── YourViewsComponent.reducers.ts
│   │   │   │   │   ├── YourViewsComponent.sagas.ts
│   │   │   │   │   ├── YourViewsComponent.scss
│   │   │   │   │   └── YourViewsComponent.test.tsx
│   │   │   │   └── index.ts
│   │   │   ├── helpers
│   │   │   │   └── index.ts
│   │   │   ├── interfaces
│   │   │   │   └── index.ts
│   │   │   ├── YourViews.actions.ts
│   │   │   ├── YourViews.constants.ts
│   │   │   ├── YourViews.reducers.ts
│   │   │   ├── YourViews.sagas.ts
│   │   │   ├── YourViews.scss
│   │   │   ├── YourViews.test.tsx
│   │   │   └── YourViews.tsx
│   ├── index.tsx
│   ├── react-app-env.d.ts
│   ├── serviceWorker.ts
│   └── setupTests.ts
├── .env
├── .env.example
├── .eslintignore
├── .eslintrc.js
├── .gitignore
├── package-lock.json
├── package.json
├── README.md
└── tsconfig.json
```

## Boilerplate Built-in Feature and Configuration

1. Login & Logout System
2. Redux, Redux Saga and Redux Persist Configuration (redux-logger included in development env only)
3. Navigation (SideBar, TopBar and Footer) with updateable menu
4. Menu in SideBar can have nested menu (children)
4. Route separation for Views that using and not using Container (SideBar, TopBar and Footer)
5. Unit Tesing example
6. Example folder for creating new Views, Cards and Components
7. Eslint

## Boilerplate Convention

### Importing Order inside Component

1. External Lib (e.g React, Reactstrap, etc)
2. Constants
3. Interfaces
4. Redux Action Type, Redux Action (since both of them declared in same file, so it will be imported from one file)
5. Helper Function
6. Custom Hook
7. Components/Cards
8. Assets
9. Style

### Code Region inside Component

1. `useDispatch` declaration
2. `useState` declaration
3. `props` object destruction
4. `useCustomHook` called here
5. `onHandleFunction` declaration
6. `useEffect` declaration
7. Return element

## How to Add New Views

1. copy and paste `example` folder inside `src > views`
2. rename the folder and file with the desired name
3. remove unwanted file
4. give your view a route: open `src > config > Routes.ts`. Instruction lies in `Routes.ts` file
5. if you need your view to be accessible from the SideBar, open `src > config > SideBarMenu.ts`, the instruction and example lies in that file
6. if your view need reducer, edit the `Example.reducers.ts` and add it into `src > config > Config.reducers.ts`
7. if your view need a sagas, edit the `Example.sagas.ts` and add it into `src > config > Config.sagas.ts`
8. if in any of your file need to call API that did not need saga, use HttpService function inside `src > helpers`

## How to Add New Shared Cards

1. open `src > cards` and copy the `example` folder. Renamed it with your desired name
2. import and export in `src > cards > index.ts`

## How to Add New Shared Components

1. open `src > components` and copy the `example` folder. Renamed it with your desired name
2. import and export in `src > components > index.ts`

## How to Add Custom Style

1. if you have global style/className, put it inside `src > scss > Custom.scss`
2. if you have global scss variables, put it inside `src > scss > Variables.scss`
